# Kanji Learning Application

Git repository containing two projects from eclipse workspace, the local database table and the slides from the presentation.

Precautions :

1. HTML pages will display chinese characters correcty. However, in your console you might see ? instead of those characters so you might want to choose UTF8 character encoder in Eclipse settings.

2. The main project is ProjectKanjiWebsite, the one you have to launch on a Tomcat server. 
The second project is kanjilearningclientgenerator was used to build the library KanjiAliveAPI-jar-with-dependencies.jar thanks to kanjialive.yaml file. That jar contrains methods and objects to retrieve data from the API Kanji Alive, you need to import it in the main project.
However, dependencies with Tomcat is weirdly managed so we had issues. Instead of adding KanjiAliveAPI-jar-with-dependencies.jar to the build path of the project (which didn't work) we added manually KanjiAliveAPI-jar-with-dependencies.jar directly in Programmes/Apache_Software_Foundation/Tomcat_9.0/lib folder. It works but it is kind of last resort so if you have a cleaner way to do it don't hesitate to do so.

3. Our local database runs on Wampserver. If you don't want to edit the persistence.xml file, note that you should create an user "root" without password with ALL PRIVILEGES. You should then create a database called 'kanjis' and import the table kanjis.sql provided in the git repository.

4. A video demo can be found on the last slide of our presentation.

